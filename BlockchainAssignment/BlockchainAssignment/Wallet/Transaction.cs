﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BlockchainAssignment.Wallet
{
    class Transaction
    {
        //create global variables
        public String hash;
        String signature;
        public String senderAddress;
        public String recipientAddress;
        public DateTime timestamp;
        public double amount;
        public double fee;
        SHA256 hasher;

        public Transaction(String from, String to, double amount, double fee, String privKey)
        {
            //intialise global variables for transaction input
            this.senderAddress = from;
            this.amount = amount;
            this.fee = fee;
            this.recipientAddress = to;
            this.timestamp = DateTime.Now;
            this.hash = CreateHash();
            //create signature using the key from, private key and the hash from create hash function
            this.signature = Wallet.CreateSignature(from, privKey, this.hash);
        }

        //copied from block file
        public String CreateHash()
        {
            String hash = String.Empty;
            
            hasher = SHA256Managed.Create();
            //hash all properties
            String input = timestamp.ToString() + senderAddress + recipientAddress + amount.ToString() + fee.ToString();
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }
        public override string ToString()
        {
            return "Transaction Hash: " + hash + "\n" +
                "Digital Signature: " + signature + "\n" +
                "Amount Transferred: " + amount.ToString() + " Assignment Coin." + "\n" + "Fees: " + fee.ToString() + "\n" +
                "Timestamp: " + timestamp + "\n" +
                "Sender Address: " + senderAddress + "\n" +
                "Receiver Address: " + recipientAddress + "\n";

        }
    }
}
