﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockchainAssignment.Wallet;

namespace BlockchainAssignment
{
    class Blockchain
    {
        public List<Block> Blocks = new List<Block>();
        public List<Transaction> transactionPool = new List<Transaction>();
        //only 5 transactions per block
        int transactionPerBlock = 5;
        public bool threadActive = false;

        public Blockchain()
        {
            //adding new block to list
            Blocks.Add(new Block());
        }
        public String getBlockString(int index)
        {
            //converting list to string
            if (index >= 0 && index < Blocks.Count)
            {
                return Blocks[index].ToString();
            }
            else
            {
                return "No such block exists";
            }
        }
        //getter for last block
        public Block getLastBlock()
        {
            return Blocks[Blocks.Count - 1];
        }

        //pending transactions getter, going through transactions in transaction pool
        //when transactions are due to be processed, they are added to a list of pending transactions known as the transaction pool
        public List<Transaction> getPendingTransaction()
        {
            int n = Math.Min(transactionPerBlock, transactionPool.Count);
            List<Transaction> transaction = transactionPool.GetRange(0, n);
            transactionPool.RemoveRange(0, n);
            return transaction;
        }
        public override string ToString()
        {
            String output = String.Empty;

            Blocks.ForEach(b => output += (b.ToString() + "\n"));
            return output;
        }
        //getting balance method
        public double GetBalance(String address)
        {
            double balance = 0.0;
            foreach(Block b in Blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if (t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount - t.fee);
                    }
                }
            }
            return balance;
        }
        //validation method for merkle root algorithm
        public bool validateMerkleRoot(Block b)
        {
            String reMarkle = Block.MerkleRoot(b.transactionList);
            return reMarkle.Equals(b.merkleRoot);

        }
    }
} 
